using System;
using System.Collections;
using UnityEngine;

namespace AssemblyCSharp
{
	public class VoxelState
	{
		public Vector3 scale;
		public Vector3 position;
		public bool active;
		
		public VoxelState(Vector3 scale, Vector3 pos, bool active)
		{
			this.scale = scale;
			this.position = pos;
			this.active = active;
		}
	}
}

