using UnityEngine;
using System.Collections;

public static class BlockScript {

	public static Transform BendTilePrefab = Resources.Load("prefabs/BendTile", typeof(Transform)) as Transform;
	public static Transform GreyTilePrefab = Resources.Load("prefabs/GreyTile", typeof(Transform)) as Transform;
	public static Transform AltTilePrefab = Resources.Load("prefabs/AltTile", typeof(Transform)) as Transform;
	public static Transform ButtonTilePrefab = Resources.Load("prefabs/ButtonTile", typeof(Transform)) as Transform;
	public static Transform CrackTilePrefab = Resources.Load("prefabs/CrackTile", typeof(Transform)) as Transform;
	public static Transform DoubleTilePrefab = Resources.Load("prefabs/DoubleTile", typeof(Transform)) as Transform;
	public static Transform HelpTilePrefab = Resources.Load("prefabs/HelpTile", typeof(Transform)) as Transform;
	public static Transform LightTilePrefab = Resources.Load("prefabs/LightTile", typeof(Transform)) as Transform;
	public static Transform QuadTilePrefab = Resources.Load("prefabs/QuadTile", typeof(Transform)) as Transform;
	public static Transform StartTilePrefab = Resources.Load("prefabs/StartTile", typeof(Transform)) as Transform;
	public static Transform EndTilePrefab = Resources.Load("prefabs/EndTile", typeof(Transform)) as Transform;
	public static Transform BlackTilePrefab = Resources.Load("prefabs/BlackTile", typeof(Transform)) as Transform;
	public static Transform TurretPrefab = Resources.Load("prefabs/Turret", typeof(Transform)) as Transform;
	public static Transform GunPrefab = Resources.Load("prefabs/Gun", typeof(Transform)) as Transform;
	public static Transform BloodPrefab = Resources.Load("prefabs/Blood", typeof(Transform)) as Transform;
	public static Transform SlopeTilePrefab = Resources.Load("prefabs/SlopeTile", typeof(Transform)) as Transform;
	public static Transform DeletTilePrefab = Resources.Load("prefabs/EditMode/DeleteTile", typeof(Transform)) as Transform;
	
	public static Transform[] BlockPrefabs =
	{
		BendTilePrefab,
		GreyTilePrefab,
		AltTilePrefab,
		ButtonTilePrefab,
		CrackTilePrefab,
		DoubleTilePrefab,
		HelpTilePrefab,
		LightTilePrefab,
		QuadTilePrefab,
		StartTilePrefab,
		EndTilePrefab,
		BlackTilePrefab,
		TurretPrefab,
		GunPrefab,
		BloodPrefab,
		SlopeTilePrefab
	};
	
	public static string[] BlockNames =
	{
		"BendTile",
		"GreyTile",
		"AltTile",
		"ButtonTile",
		"CrackTile",
		"DoubleTile",
		"HelpTile",
		"LightTile",
		"QuadTile",
		"StartTile",
		"EndTile",
		"BlackTile",
		"Turret",
		"Gun",
		"Blood",
		"SlopeTile"
	};
	
	
	public static Material BendTileMaterial = Resources.Load("material/BendTileMaterial", typeof(Material)) as Material;
	public static Material BlackTileMaterial = Resources.Load("material/BlackTileMaterial", typeof(Material)) as Material;
	public static Material ButtonTileMaterial = Resources.Load("material/ButtonTileMaterial", typeof(Material)) as Material;
	public static Material GreyTileAltMaterial = Resources.Load("material/GreyTileAltMaterial", typeof(Material)) as Material;
	public static Material GreyTileBloodyMaterial = Resources.Load("material/GreyTileBloodyMaterial", typeof(Material)) as Material;
	public static Material GreyTileCrackedMaterial = Resources.Load("material/GreyTileCrackedMaterial", typeof(Material)) as Material;
	public static Material GreyTileDoubleMaterial = Resources.Load("material/GreyTileDoubleMaterial", typeof(Material)) as Material;
	public static Material GreyTileMaterial = Resources.Load("material/GreyTileMaterial", typeof(Material)) as Material;
	public static Material GreyTileQuadMaterial = Resources.Load("material/GreyTileQuadMaterial", typeof(Material)) as Material;
	public static Material LightTileMaterial = Resources.Load("material/LightTileMaterial", typeof(Material)) as Material;
	
	public static Material[] Materials =
	{
		BendTileMaterial,
		BlackTileMaterial,
		ButtonTileMaterial,
		GreyTileAltMaterial,
		GreyTileBloodyMaterial,
		GreyTileCrackedMaterial,
		GreyTileDoubleMaterial,
		GreyTileMaterial,	
		GreyTileQuadMaterial,
		LightTileMaterial,
	};
}
