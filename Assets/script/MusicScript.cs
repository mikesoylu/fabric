using UnityEngine;
using System.Collections;

public class MusicScript : MonoBehaviour {
	
	// We have one "Music" gameObject in the scene.
	// If the scene changes, another one will be created BELOW the older one.
	// Check if the older one is still playing.
	// If it is, just destroy, older one will continue playing
	
	void Awake()
	{
		AudioSource music = GameObject.Find("Music").GetComponent<AudioSource>();
		if(music.isPlaying)
		{
			Destroy(gameObject);
		}
		else
		{
			DontDestroyOnLoad(gameObject);
			music.Play();
		}
	}
}
