using UnityEngine;
using System.Collections;
using System;

public class FlashScript : MonoBehaviour
{
	private float counter = 0;
	private const float timeout = 2f;
	
	public void Flash()
	{
		counter = 0;
		guiTexture.color = new Color(1f,1f,1f,1f);
	}
	
	void Start()
	{
		counter = 0;
		audio.Play();
	}

	void Update()
	{
		if (counter > timeout)
		{
			guiTexture.color = new Color(1f,1f,1f,0f);
			return;
		}
		counter += Time.deltaTime;
		guiTexture.color = new Color(1f,1f,1f,1f-counter/timeout);

	}
}
