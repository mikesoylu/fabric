using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using AssemblyCSharp;

public class LevelLoader_v2 : MonoBehaviour
{
	public Transform VoxelPrefab;
	public Transform VoxelLightPrefab;
	public Transform BendZonePrefab;
	public Transform Player;
	public Transform TurretPrefab;
	public Transform GunPrefab;
	public Transform EndPrefab;
	
	public Material GreyTileMaterial;
	public Material GreyTileAltMaterial;
	public Material GreyTileDoubleMaterial;
	public Material GreyTileQuadMaterial;
	public Material BloodyTileMaterial;
	public Material CrackedTileMaterial;
	public Material LightTileMaterial;
	public Material BendTileMaterial;
	public Material StartTileMaterial;
	public Material HelpTileMaterial;
	public Material ButtonTileMaterial;
	
	public Transform FPSCamera;
	
	private const int MAP_WIDTH = 32;
	private const int MAP_HEIGHT = 16;
	private const int MAP_DEPTH = 32;
	
	public static int LEVEL_NUM = 1;
	public static int LAST_LEVEL = 10;
	
	private bool LEVEL_HAS_BUTTON = false;
	private bool BUTTON_PRESSED = false;
	
	private bool LEVEL_HAS_GUN = false;
	public static bool GUN_AQUIRED = false; // TODO: AQUIRED.
	
	// are we playing user levels
	public static bool USER_MODE = false;
	
	
	public enum Tiles : byte { EMPTY = 0, GREY, BLOOD, CRACK, LIGHT, END, HELP, START, BEND, GUN, TURRET, ALT, DOUBLE, QUAD, BUTTON };
	
	private List<Object> voxels;
	
	private Transform endTile;
	
	void Start()
	{
		
		voxels = new List<Object>();
		
		LoadMapFromFile();
		
		if (LEVEL_HAS_GUN)
			GUN_AQUIRED = false;
		else 
			GUN_AQUIRED = true;
		
		if(GUN_AQUIRED)
			Debug.Log("Gun Acquired!");
		
		if (LEVEL_HAS_BUTTON)
		{
			BUTTON_PRESSED = false;
		} else 
		{
			PressButton();
		}
		
		Debug.Log("Level: " + LEVEL_NUM);
		Screen.lockCursor = true;
		Screen.showCursor = false;
	}
	public bool RevertMap()
	{
		bool ret = false;
		foreach(Object vx in voxels)
		{
			if (vx is Transform)
			{
				ret = ((Transform)vx).gameObject.GetComponent<VoxelScript>().Revert();
			} else if (vx is GameObject)
			{
				ret = ((GameObject)vx).GetComponent<VoxelScript>().Revert();
			} else
			{
				Debug.Log("OOPS");
			}
		}
		return ret;
	}
	public bool PressButton()
	{
		if (BUTTON_PRESSED)
			return false;
		
		BUTTON_PRESSED = true;
		
		for (int l = 1; l<5; l++)
		{
			Object nv = Instantiate(EndPrefab,
						 new Vector3(endTile.position.x,endTile.position.y+l,endTile.position.z),
						 Quaternion.identity);
			((Transform)nv).transform.localEulerAngles = new Vector3(-90,0,0);
			((Transform)nv).transform.parent = endTile;
			// only do it for the first
			if (l == 1)
			{
				((Transform)nv).transform.tag = "ender";
			}
			((Transform)nv).collider.isTrigger = true;
			
		}
		return true;
	}
	public bool ShiftX(float lower, float upper, byte typeA = 0, byte typeB = 0)
	{
		float eps = 0.01f;
		Plane low = new Plane(new Vector3(1,0,0), new Vector3(lower+eps,0,0));
		Plane hi = new Plane(new Vector3(1,0,0), new Vector3(upper-eps,0,0));
		
		foreach(Object vx in voxels)
		{
			Transform xfrm = null;
			VoxelScript vs = null;
			GameObject go = null;
			if (vx is Transform)
			{
				xfrm = (Transform)vx;
				vs = ((Transform)vx).gameObject.GetComponent<VoxelScript>();
				go = ((Transform)vx).gameObject;
			} else if (vx is GameObject)
			{
				xfrm = ((GameObject)vx).transform;
				vs = ((GameObject)vx).GetComponent<VoxelScript>();
				go = (GameObject)vx;
			} else
			{
				Debug.Log("OOPS");
			}
			if (!low.GetSide(xfrm.position))
			{
				vs.Apply(new VoxelState(xfrm.localScale,
								new Vector3(xfrm.position.x-(lower - upper + 1)*0.5f, xfrm.position.y, xfrm.position.z),
								go.active));
				vs.flicker = false;
				
			} else if (hi.GetSide(xfrm.position))
			{
				vs.Apply(new VoxelState(xfrm.localScale,
								new Vector3(xfrm.position.x+(lower - upper + 1)*0.5f, xfrm.position.y, xfrm.position.z),
								go.active));
				vs.flicker = false;
			} else
			{
				vs.Apply(new VoxelState(new Vector3(0f, xfrm.localScale.y, xfrm.localScale.z),
								new Vector3((lower + upper)*0.5f, xfrm.position.y, xfrm.position.z),
								false));
				vs.flicker = true;
			}
		}
		return true;
	}
	public bool ShiftY(float lower, float upper, byte typeA = 0, byte typeB = 0)
	{
		float eps = 0.01f;
		Plane low = new Plane(new Vector3(0,1,0), new Vector3(0,lower+eps,0));
		Plane hi = new Plane(new Vector3(0,1,0), new Vector3(0,upper-eps,0));
		
		foreach(Object vx in voxels)
		{
			Transform xfrm = null;
			VoxelScript vs = null;
			GameObject go = null;
			if (vx is Transform)
			{
				xfrm = (Transform)vx;
				vs = ((Transform)vx).gameObject.GetComponent<VoxelScript>();
				go = ((Transform)vx).gameObject;
			} else if (vx is GameObject)
			{
				xfrm = ((GameObject)vx).transform;
				vs = ((GameObject)vx).GetComponent<VoxelScript>();
				go = (GameObject)vx;
			} else
			{
				Debug.Log("OOPS");
			}
			if (!low.GetSide(xfrm.position))
			{
				vs.Apply(new VoxelState(xfrm.localScale,
								new Vector3(xfrm.position.x, xfrm.position.y-(lower - upper + 1)*0.5f, xfrm.position.z),
								go.active));
				vs.flicker = false;
				
			} else if (hi.GetSide(xfrm.position))
			{
				vs.Apply(new VoxelState(xfrm.localScale,
								new Vector3(xfrm.position.x, xfrm.position.y+(lower - upper + 1)*0.5f, xfrm.position.z),
								go.active));
				vs.flicker = false;
			} else
			{
				vs.Apply(new VoxelState(new Vector3(xfrm.localScale.x, 0f, xfrm.localScale.z),
								new Vector3(xfrm.position.x, (lower + upper)*0.5f, xfrm.position.z),
								false));
				vs.flicker = true;
			}
		}
		return true;
	}
	public bool ShiftZ(float lower, float upper, byte typeA = 0, byte typeB = 0)
	{
		float eps = 0.01f;
		Plane low = new Plane(new Vector3(0,0,1), new Vector3(0,0,lower+eps));
		Plane hi = new Plane(new Vector3(0,0,1), new Vector3(0,0,upper-eps));
		
		foreach(Object vx in voxels)
		{
			Transform xfrm = null;
			VoxelScript vs = null;
			GameObject go = null;
			if (vx is Transform)
			{
				xfrm = (Transform)vx;
				vs = ((Transform)vx).gameObject.GetComponent<VoxelScript>();
				go = ((Transform)vx).gameObject;
			} else if (vx is GameObject)
			{
				xfrm = ((GameObject)vx).transform;
				vs = ((GameObject)vx).GetComponent<VoxelScript>();
				go = (GameObject)vx;
			} else
			{
				Debug.Log("OOPS");
			}
			if (!low.GetSide(xfrm.position))
			{
				vs.Apply(new VoxelState(xfrm.localScale,
								new Vector3(xfrm.position.x, xfrm.position.y, xfrm.position.z-(lower - upper + 1)*0.5f),
								go.active));
				vs.flicker = false;
				
			} else if (hi.GetSide(xfrm.position))
			{
				vs.Apply(new VoxelState(xfrm.localScale,
								new Vector3(xfrm.position.x, xfrm.position.y, xfrm.position.z+(lower - upper + 1)*0.5f),
								go.active));
				vs.flicker = false;
			} else
			{
				vs.Apply(new VoxelState(new Vector3(xfrm.localScale.x, xfrm.localScale.y, 0f),
								new Vector3(xfrm.position.x, xfrm.position.y, (lower + upper)*0.5f),
								false));
				vs.flicker = true;
			}
		}
		return true;
	}
	
	void LoadMapFromFile()
	{
		StreamReader sr;
		if (USER_MODE)
		{
			sr = new StreamReader(Application.dataPath + "/user_levels/level"+LEVEL_NUM+".txt");
		} else
		{
			//TextAsset levelfile = Resources.Load("levels/level" + LEVEL_NUM) as TextAsset;
			TextAsset levelfile = Resources.Load("levels/import_test") as TextAsset;
			MemoryStream ms = new MemoryStream(levelfile.bytes);
			sr = new StreamReader(ms);
		}
		// skip header
		for(int l = 0; l < 10; l++)
			sr.ReadLine();
		
		// read num voxels
		int numVox = int.Parse(sr.ReadLine());
		
		/* skip marker voxels
		sr.ReadLine();
		sr.ReadLine();
		sr.ReadLine();*/
		
		// init level
		for (int i = 0; i<numVox; i++)
		{
			//Debug.Log (i);
			string lnstr = sr.ReadLine();
			if(lnstr == null)
				break;
			string[] line = lnstr.Split(' ');
			int vx = int.Parse(line[0])-100;
			int vy = int.Parse(line[1])-100;
			int vz = int.Parse(line[2])-100;
			string type = line[3];
			
			int layerNum = int.Parse(line[4]);
			
			Transform nv;
			Light[] lights;
			
			// lights
			if (type == "Light") 		// light
			{
				
				nv = Instantiate(VoxelLightPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				voxels.Add(nv);
				nv.gameObject.renderer.material = LightTileMaterial;
				lights = nv.gameObject.GetComponentsInChildren<Light>();
    			foreach (Light l in lights)
				{
    				l.enabled = true;
				}
				
			} else if (type == "Start")	// start
			{
				Player.transform.position = new Vector3(vx, vy+2, vz);
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				voxels.Add(nv);
				nv.gameObject.renderer.material = GreyTileMaterial;
				
			} else if (type == "Bend")	// bend
			{
				nv = Instantiate(VoxelLightPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				voxels.Add(nv);
				nv.tag = "bender";
				nv.gameObject.GetComponent<ParticleSystem>().enableEmission = true;
				nv.gameObject.renderer.material = BendTileMaterial;
				lights = nv.gameObject.GetComponentsInChildren<Light>();
    			foreach (Light l in lights)
				{
    				l.enabled = true;
					l.color = new Color(1f, 0.5f, 0.5f);
				}
				
			} else if (type == "End")	// end
			{
				nv = Instantiate(VoxelLightPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				voxels.Add(nv);
				endTile = nv;
				nv.gameObject.renderer.material = StartTileMaterial;
				lights = nv.gameObject.GetComponentsInChildren<Light>();
    			foreach (Light l in lights)
				{
	        		l.enabled = true;
					l.color = new Color(0.5f, 0.5f, 1f);
				}
				
			} else if (type == "Turret")	// turret
			{
				nv = Instantiate(TurretPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				nv.gameObject.GetComponentInChildren<TurretScript>().SetPlayer(Player);
				nv.gameObject.GetComponentInChildren<TurretScript>().SetCamera(FPSCamera);
				voxels.Add(nv);
				
			} else if (type == "Gun")	// gun
			{
				nv = Instantiate(GunPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				LEVEL_HAS_GUN = true;
				
				Debug.Log("level has gun");
				
			} else if (type == "Button")	// button tile
			{
				nv = Instantiate(VoxelLightPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				voxels.Add(nv);
				nv.gameObject.renderer.material = ButtonTileMaterial;
				nv.tag = "buttoner";
				lights = nv.gameObject.GetComponentsInChildren<Light>();
	    		foreach (Light l in lights)
				{
        			l.enabled = true;
					l.color = new Color(0.5f, 0.5f, 1f);
				}
				LEVEL_HAS_BUTTON = true;
				BUTTON_PRESSED = false;
				Debug.Log("level has button");
				
			} else if (type == "Crack")	// crack tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				voxels.Add(nv);
				nv.gameObject.renderer.material = CrackedTileMaterial;
				
			} else if (type == "Quad")	// quad tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				voxels.Add(nv);
				nv.gameObject.renderer.material = GreyTileQuadMaterial;
				
			} else if (type == "Double")	// double tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				voxels.Add(nv);
				nv.gameObject.renderer.material = GreyTileDoubleMaterial;
				
			} else if (type == "Help")	// help tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				voxels.Add(nv);
				nv.gameObject.renderer.material = HelpTileMaterial;
				
			} else if (type == "Blood")	// blood tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				voxels.Add(nv);
				nv.gameObject.renderer.material = BloodyTileMaterial;
				
			} else if (type == "Alt") // alt tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				voxels.Add(nv);
				nv.gameObject.renderer.material = GreyTileAltMaterial;
				
			} else 							// grey tile
			{
				nv = Instantiate(VoxelPrefab,
								 new Vector3(vx,vy,vz),
								 Quaternion.identity) as Transform;
				voxels.Add(nv);
				nv.gameObject.renderer.material = GreyTileMaterial;
			}
		}
		sr.Close();
	}
}
