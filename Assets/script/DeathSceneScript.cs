using UnityEngine;
using System.Collections;

public class DeathSceneScript : MonoBehaviour
{

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
			Application.LoadLevel("Splash");
		else if (Input.GetKey(KeyCode.Space))
		{
			if (LevelLoader.LEVEL_NUM == LevelLoader.LAST_LEVEL)
			{
				Application.LoadLevel("Splash");
				LevelLoader.LEVEL_NUM = 1;
			} else
				Application.LoadLevel("GameScene");
		}
	}
}
