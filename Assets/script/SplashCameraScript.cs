using UnityEngine;
using System.Collections;

public class SplashCameraScript : MonoBehaviour
{
	public float targetY;
	public Transform spaceText;
	public bool UserLevelsEnabled = false;
	
	private bool buttonsVisible = false;
	
	private int buttonWidth = 150;
	private int buttonHeight = 50;
	private int buttonTopPadding = 600;
	private int buttonLeftPadding = 250;
	
	void FixedUpdate()
	{
		Vector3 p = transform.position;
		p.y += (targetY-p.y)*0.01f;
		transform.position = p;
	}
	
	void OnGUI()
	{
		if(buttonsVisible)
		{
			if (GUI.Button(new Rect(buttonLeftPadding,buttonTopPadding,buttonWidth,buttonHeight),"Start"))
			{
				print("clicked start");
			}
			if (GUI.Button(new Rect(buttonLeftPadding+350,buttonTopPadding,buttonWidth,buttonHeight),"Edit"))
			{
				print("clicked edit");
			}
            
		}
	}
	
	void Update()
	{
		if (transform.position.y > targetY-1)
		{
			//spaceText.gameObject.SetActiveRecursively(true);
			buttonsVisible = true;
		}
		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
		else if (Input.GetKey(KeyCode.U) && UserLevelsEnabled)
		{
			Application.LoadLevel("GameScene");
			if (!LevelLoader.USER_MODE)
				LevelLoader.LEVEL_NUM = 1;
			LevelLoader.USER_MODE = true;
		} else if (Input.GetKey(KeyCode.Space))
		{
			Application.LoadLevel("GameScene");
			if (LevelLoader.USER_MODE)
				LevelLoader.LEVEL_NUM = 1;
			LevelLoader.USER_MODE = false;
		}
	}
}
