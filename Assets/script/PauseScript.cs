﻿using UnityEngine;
using System.Collections;

public class PauseScript : MonoBehaviour {
	
	bool isPaused;
	bool isTesting;
	
	void Start () 
	{
		isTesting = GameObject.Find("IsTestingLevel") != null;
	}
	
	void SetController(bool arg)
	{
		GameObject fps = GameObject.Find("First Person Controller");
		fps.GetComponent<MouseLook>().enabled = arg;
		fps.GetComponent<FPSInputController>().enabled = arg;
		fps.GetComponent<CharacterController>().enabled = arg;
		Screen.showCursor = !arg;
	}
	
	void OnGUI()
	{
		if(isPaused)
		{
			int left = Screen.width/4;
			int top = Screen.height/4;
			int width = Screen.width/2;
			int height = Screen.height/2;
			
			GUI.Box(new Rect(left,top,width,height), "");
			
			if(GUI.Button(new Rect(Screen.width/2 - 50, Screen.height / 2 ,100,40),"return build"))
			{
				Application.LoadLevel("EditScene");
			}
		}
	}
	
	void Update () 
	{
		if(isTesting)
		{
			if(Input.GetKeyDown(KeyCode.Escape))
			{
				isPaused = !isPaused;
				SetController(!isPaused);
			}
		}
	}
}
