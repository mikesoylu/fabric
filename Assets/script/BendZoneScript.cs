using UnityEngine;
using System.Collections;
using System;

public class BendZoneScript : MonoBehaviour
{
	private float counter = 0;
	private const float timeout = 0.2f;
	void Start()
	{
		counter = 0;
	}
	
	void Update()
	{
		counter += Time.deltaTime;
		if (counter > timeout)
		{
			Destroy(gameObject);
		}
	}
}
