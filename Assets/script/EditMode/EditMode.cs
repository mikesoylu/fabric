using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class EditMode : MonoBehaviour 
{
	public GameObject text;
	public Transform cam;
	public float floatingSpeed = 0.1f;
	
	private const int MAX_SIZE = 200; // In one dimension
	private const int MAX_LAYER_COUNT = 9;
	private int initialSize = 20; // Must be an even number
	
	private List<Transform> map;
	private bool[,,] mapSlots; // Fast access to check whether a slot is occupied
	private RaycastHit hit;
	private Vector3 blockPos;
	
	private int selection = 1;
	private int currLayer = 1;
	
	private Transform ghostBlock;
	
	private bool isLineBuilding = false;
	private List<Transform> lineGhosts;
	private Transform lineAnchor;
	
	private int blockRot = 0;
	
	private bool isPaused = false;
	private bool isShowingBlocks = false;
	private bool isDeletingBlocks = false;
	
	private int buttonWidth = 100;
	private int buttonHeight = 40;
	
	void Start() 
	{   
		map = new List<Transform>();
		mapSlots = new bool[MAX_SIZE*2, MAX_SIZE*2, MAX_SIZE*2];
		UpdateText(BlockScript.BlockNames[selection],currLayer);
		
		GameObject fpsCam = GameObject.Find("First Person Controller") as GameObject;
		if(fpsCam != null)
		{
			fpsCam.transform.position = new Vector3(0, initialSize/2, initialSize/2);
		}
		
		lineGhosts = new List<Transform>();
		lineAnchor = new GameObject("LineAnchor").transform;
		
		Vector3 startTilePos = new Vector3(initialSize/2, 0f, initialSize/2);
		SetGhostBlock(BlockScript.BlockPrefabs[selection], startTilePos + new Vector3(0f,1f,0f), blockRot);
		
		if(GameObject.Find("IsTestingLevel") != null)
		{
			// We came from playscene
			string path = Application.dataPath + "/Resources/levels/testLevel.txt";
			map = gameObject.GetComponent<LevelLoader_v3>().LoadFromFile(path, true);
		}
		else
		{
			// First run
			AddEditBlock(startTilePos, Quaternion.identity, 9, 1);
			
			for(float x = 0f; x < initialSize + 1; x++)
			{
				for(float z = 0f; z < initialSize + 1; z++) 
				{
					if(x == startTilePos.x && z == startTilePos.z)
						continue;
					
					AddEditBlock(new Vector3(x, 0f, z), Quaternion.identity, 1, 1);
				}
			}
		}
	}
	
	void OnGUI()
	{
		if(isPaused)
		{
			int left = Screen.width/4;
			int top = Screen.height/4;
			int width = Screen.width/2;
			int height = Screen.height/2;
			
			GUI.Box(new Rect(left,top,width,height), "");
			
			int buttonLeft = Screen.width/2 - buttonWidth/2;
			
			if(DrawButton(buttonLeft, Screen.height / 2 - (buttonHeight+10) * 2, "Save"))
			{
				SaveToFile();
				isPaused = false;
			}
			if(DrawButton(buttonLeft, Screen.height / 2 - (buttonHeight+10) * 1, "Load"))
			{
				LoadFromFile();
				isPaused = false;
			}
			if(DrawButton(buttonLeft, Screen.height / 2 - (buttonHeight+10) * 0, "Test Level"))
			{
				// This GameObject will persist through all level building and testing cycle, until the app is closed.
				if(GameObject.Find("IsTestingLevel") == null)
				{
					GameObject go = new GameObject("IsTestingLevel");
					DontDestroyOnLoad(go);
				}
				
				WriteToFile(Application.dataPath + "/Resources/levels/testLevel.txt");
				Application.LoadLevel("GameScene");
			}
		}
		
		else if(isShowingBlocks)
		{
			int left = Screen.width/4;
			int top = Screen.height/4;
			int width = Screen.width/2;
			int height = Screen.height/2;
			
			GUI.Box(new Rect(left,top,width,height), "");
			
			int leftPadding = 10;
			int topPadding = 10;
			for(int k = 0; k < BlockScript.BlockNames.Length; k++, topPadding += 40)
			{
				if(k != 0 && (k%5) == 0)
				{
					leftPadding += 110;
					topPadding = 10;
				}
				
				if(DrawButton(left+leftPadding,top+topPadding,BlockScript.BlockNames[k]))
				{
					ChangeBlock(k);
					isShowingBlocks = false;
					break;
				}
			}
		}
	}
	
	bool DrawButton(int left, int top, string label)
	{
		return GUI.Button(new Rect(left,top,buttonWidth,buttonHeight),label);
	}
	
	void UpdateText(string blockName, int layer)
	{
		text.GetComponent<GUIText>().text = " " + blockName + " \n Layer: " + layer.ToString();
	}
	
	void Update() 
	{
		if(!isShowingBlocks)
		{
			Ray r = new Ray(cam.transform.position, cam.transform.forward);
			if(Physics.Raycast(r, out hit)) 
			{
				blockPos = hit.transform.position + hit.normal;
				
				// Placing ghost block
				if(hit.transform.gameObject.layer != 2)
				{
					if(ghostBlock.transform.position != hit.transform.position)
					{
						ghostBlock.position = isDeletingBlocks ? hit.transform.position : blockPos;
						ghostBlock.rotation = Quaternion.Euler(0, blockRot*90, 0);	
					}
					
				}
				
				// Block deletion
				if(isDeletingBlocks && Input.GetMouseButtonDown(0)) 
				{
					if(currLayer == 0)
					{
						return;
					}
					map.Remove(hit.transform);
					Destroy(hit.transform.gameObject);
					SetSlot(hit.transform.position, false);
				}
				
				// Line instantiation
				if(isLineBuilding)
				{
					if(hit.transform.position != ghostBlock.position)
					{
						SetLineGhosts("xz");
					}
				}
			}
			
			// Adding block, even if our ray doesn't hit something
			// Because ghost is already set somewhere
			if(!isDeletingBlocks && Input.GetMouseButtonDown(0)) 
			{
				//AddEditBlock(blockPos, Quaternion.Euler(0, blockRot*90, 0), selection, currLayer);
				lineAnchor.position = ghostBlock.position;
				isLineBuilding = true;
			}
			if(!isDeletingBlocks && Input.GetMouseButtonUp(0)) 
			{
				InstantiateLineGhosts();
				isLineBuilding = false;
			}
		}
		
		if(!isPaused)
		{
			KeyboardInput();
		}
		
		// This better be seperate
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			isPaused = !isPaused;
			SetController(!isPaused);
		}
	}
	
	void SetController(bool arg)
	{
		GameObject fps = GameObject.Find("First Person Controller");
		fps.GetComponent<MouseLook>().enabled = arg;
		fps.GetComponent<FPSInputController>().enabled = arg;
		fps.GetComponent<CharacterController>().enabled = arg;
		Screen.showCursor = !arg;
	}
	
	void SetSlot(Vector3 blockPos, bool val)
	{
		Vector3 i = BlockSlotIndices(blockPos);
		
		int iX = Mathf.FloorToInt(i.x);
		int iY = Mathf.FloorToInt(i.y);
		int iZ = Mathf.FloorToInt(i.z);
		
		print("setting slot at: "+iX+" "+iY+" "+iZ);
		
		mapSlots[iX, iY, iZ] = val;
	}
	
	bool GetSlot(Vector3 blockPos)
	{
		Vector3 i = BlockSlotIndices(blockPos);
		
		int iX = Mathf.FloorToInt(i.x);
		int iY = Mathf.FloorToInt(i.y);
		int iZ = Mathf.FloorToInt(i.z);
		
		return mapSlots[iX, iY, iZ];
	}
	
	Vector3 BlockSlotIndices(Vector3 blockPos)
	{
		int absX = Mathf.FloorToInt(Mathf.Abs(blockPos.x));
		int absY = Mathf.FloorToInt(Mathf.Abs(blockPos.y));
		int absZ = Mathf.FloorToInt(Mathf.Abs(blockPos.z));
		
		int iX = blockPos.x >= 0 ? absX * 2  : absX * 2 - 1;
		int iY = blockPos.y >= 0 ? absY * 2  : absY * 2 - 1;
		int iZ = blockPos.z >= 0 ? absZ * 2  : absZ * 2 - 1;
		
		return new Vector3(iX, iY, iZ);
	}
	
	void AddEditBlock(Vector3 pos, Quaternion rot, int type, int layer)
	{	
		Transform block = gameObject.GetComponent<LevelLoader_v3>().InstantiateEditBlock(pos,rot,type,layer,currLayer);
		map.Add(block);
		SetSlot(block.position, true);
	}
	
	void SetGhostBlock(Object prefab, Vector3 pos, int rot)
	{
		if(ghostBlock)
		{	
			Destroy(ghostBlock.gameObject);
		}
		
		ghostBlock = Instantiate(prefab, pos, Quaternion.Euler(0,rot*90,0)) as Transform;
		ghostBlock.gameObject.renderer.material.color = new Color(1f,0f,0f);
		ghostBlock.tag = "ghost";
		ghostBlock.gameObject.layer = 2;
		
	}
	
	// TODO: Add other dimensions
	void SetLineGhosts(string lineAxis) 
	{
		foreach(Transform block in lineGhosts)
		{
			Destroy(block.gameObject);
		}
		lineGhosts.Clear();
		
		if (lineAxis == "xz")
		{
			bool sideBoolX = lineAnchor.position.x < ghostBlock.position.x;
			bool sideBoolZ = lineAnchor.position.z < ghostBlock.position.z;
			
			for(float x = lineAnchor.position.x; 
				sideBoolX ? x <= ghostBlock.position.x : x >= ghostBlock.position.x; 
				x = sideBoolX ? x+1 : x-1)
			{
				
				for(float z = lineAnchor.position.z; 
					sideBoolZ ? z < ghostBlock.position.z : z >= ghostBlock.position.z; 
					z = sideBoolZ ? z+1 : z-1)
				{
					Vector3 blockPos = new Vector3(x, lineAnchor.position.y, z);
					
					if(GetSlot(blockPos) == false) // If the slot is empty
					{
						Transform ghostLineBlock = Instantiate(
							BlockScript.BlockPrefabs[selection], 
							blockPos,
							Quaternion.Euler(0, blockRot*90, 0)) as Transform;
						
						ghostLineBlock.gameObject.layer = 2;
						
						ghostLineBlock.gameObject.renderer.material.color = Color.blue;
						
						lineGhosts.Add(ghostLineBlock);	
					}
				}
			}
		}
	}
	
	void InstantiateLineGhosts()
	{
		if(lineGhosts.Count == 0) // Just one click. (No drag)
		{
			AddEditBlock(ghostBlock.position, Quaternion.Euler(0, blockRot*90, 0), selection, currLayer);
		}
		else
		{
			foreach(Transform ghostLineBlock in lineGhosts)
			{
				AddEditBlock(ghostLineBlock.position, Quaternion.Euler(0, blockRot*90, 0), selection, currLayer);
			}
			
			foreach(Transform block in lineGhosts)
			{
				Destroy(block.gameObject);
			}
			lineGhosts.Clear();
		}
	}
	
	void ChangeLayer(int c) 
	{
		GameObject map = GameObject.Find("Map");
		if(c == 0) 
		{
			foreach(MeshRenderer rend in map.GetComponentsInChildren<MeshRenderer>()) 
			{
				rend.enabled = true;	
			}
			return;
		}
		
		else 
		{
			foreach(MeshRenderer rend in map.GetComponentsInChildren<MeshRenderer>()) 
			{
				rend.enabled = false;	
			}	
		}
		
		string name = "Layer " + c.ToString();
		currLayer = c;
		UpdateText(BlockScript.BlockNames[selection],c);
		map = GameObject.Find(name);
		
		foreach(MeshRenderer rend in map.GetComponentsInChildren<MeshRenderer>()) 
		{
			rend.enabled = true;
		}
	}
	
	void ChangeBlock(int c) 
	{	
		selection = c;
		UpdateText(BlockScript.BlockNames[c],currLayer);
		SetGhostBlock(BlockScript.BlockPrefabs[selection], ghostBlock.position, blockRot);
	}
	
	void KeyboardInput()
	{
		// Block selection
		if(Input.GetKeyDown(KeyCode.G))
		{
			isShowingBlocks = true;
			SetController(false);
		}
		if(Input.GetKeyUp(KeyCode.G))
		{
			isShowingBlocks = false;
			SetController(true);
		}
		
		// Change layer
		if(Input.GetKeyDown(KeyCode.Q)) 
		{
			currLayer--;
			if(currLayer == 0)
				currLayer = MAX_LAYER_COUNT-1;
			ChangeLayer(currLayer);
		}
		
		if(Input.GetKeyDown(KeyCode.E)) 
		{
			currLayer++;
			if(currLayer == MAX_LAYER_COUNT)
				currLayer = 1;
			ChangeLayer(currLayer);
		}
		
		// Change rotation
		if(Input.GetKeyDown(KeyCode.Z)) 
		{
			blockRot--;
			blockRot %= 4;
		}
		if(Input.GetKeyDown(KeyCode.X)) 
		{
			blockRot++;
			blockRot %= 4;
		}
		
		// Line building
		if(Input.GetKeyDown(KeyCode.L))
		{
			lineAnchor.position = ghostBlock.position;
			isLineBuilding = true;
		}
		if(Input.GetKeyUp(KeyCode.L))
		{
			InstantiateLineGhosts();
			isLineBuilding = false;
		}
		
		// Cam Controls
		if(Input.GetKey (KeyCode.Space)) 
		{
			cam.Translate(Vector3.up * floatingSpeed, Space.World);	
		}
		
		if(Input.GetKey (KeyCode.LeftControl)) 
		{
			cam.Translate(Vector3.down * floatingSpeed, Space.World);
		}
		
		if(Input.GetAxis("Mouse ScrollWheel") < 0)
		{
			cam.transform.position -= cam.transform.forward;
		}
		if(Input.GetAxis("Mouse ScrollWheel") > 0)
		{
			cam.transform.position += cam.transform.forward;
		}
		
		// Block deletion
		if(Input.GetKeyDown(KeyCode.LeftShift) && hit.transform != null)
		{
			isDeletingBlocks = true;
			SetGhostBlock(BlockScript.DeletTilePrefab, hit.transform.position, blockRot);
		}
		
		if(Input.GetKeyUp(KeyCode.LeftShift))
		{
			isDeletingBlocks = false;
			SetGhostBlock(BlockScript.BlockPrefabs[selection], ghostBlock.position, blockRot);
		}
		
		// Save-Load
		if(Input.GetKeyDown(KeyCode.F2)) 
		{
			SaveToFile();
		}
		
		if(Input.GetKeyDown(KeyCode.F3))
		{
			LoadFromFile();
		}
	}
	
	void SaveToFile()
	{
		string path = EditorUtility.SaveFilePanel("Save to file", "", "UntitledLevel", "txt");
		WriteToFile(path);
	}
	
	void WriteToFile(string path)
	{
		if(path.Length != 0)
		{
			int block_count; // TODO: gerekli olcak mi bu?
			GameObject mapObject = GameObject.Find("Map");
			
			using(StreamWriter file = new StreamWriter(path))
			{
				for(int i=0; i<10; i++) 
				{
					file.WriteLine("");
				}
				
				block_count = mapObject.GetComponentsInChildren<BlockInfo>().Length;
				
				file.WriteLine(block_count.ToString());
				
				foreach(BlockInfo i in mapObject.GetComponentsInChildren<BlockInfo>()) 
				{
					file.WriteLine(i.info);
				}
			}
		}
	}
	
	void LoadFromFile()
	{
		string path = EditorUtility.OpenFilePanel("Open from file", "", "txt");
			
		if(path.Length != 0)
		{
			// Cleanup old map
			foreach(Transform block in map)
			{
				Destroy(block.gameObject);
			}
			map.Clear();
			
			map = gameObject.GetComponent<LevelLoader_v3>().LoadFromFile(path, true);
		}
	}
}