using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour
{
	public GameObject FlashLayer;
	public GameObject BloodLayer;
	
	public void setStartPosition(Transform startTile)
	{
		transform.position = new Vector3(startTile.position.x, startTile.position.y+10, startTile.position.z);
	}
	
	void FixedUpdate()
	{
		if (transform.position.y < -30)
		{
			BloodLayer.GetComponent<BloodScript>().Hit(true);
		}
	}
	void OnTriggerEnter(Collider coll)
	{
	
	  	if (coll.gameObject.tag == "ender")
		{
			coll.gameObject.collider.isTrigger = false;
			LevelLoader.LEVEL_NUM++;
			FlashLayer.GetComponent<FlashScript>().Flash();
			Application.LoadLevel("GameScene");
		}
	  	if (coll.gameObject.tag == "gunner")
		{
			GameObject.Find("GlobalScripts").GetComponent<MapScript>().AcquireGun();	
		}
	}
}
