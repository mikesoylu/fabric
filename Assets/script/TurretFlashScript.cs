using UnityEngine;
using System.Collections;

public class TurretFlashScript : MonoBehaviour
{
	void Update()
	{
		float s = (Mathf.Sin(Time.time*50.0f)+1)*0.025f;
		transform.localScale = new Vector3(s,s,s);
		transform.RotateAround(transform.up, Time.deltaTime*10.0f);
	}
}
