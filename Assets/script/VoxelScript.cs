using UnityEngine;
//using System;
using System.Collections;
using System.Collections.Generic;

using AssemblyCSharp;

public class VoxelScript:MonoBehaviour
{
	public byte type;
	public Stack<VoxelState> history;
	public bool disableLights = false;
	
	public bool flicker = false;
	private float flickerDuration;
	private float flickerStartTime;
	private bool isFlickerMaterial;
	public Material flickerMaterial;
	private Material origMaterial;
	
	public class Tween
	{
		public VoxelState start;
		public VoxelState end;
		public float percent;
		public Tween(VoxelState s, VoxelState e)
		{
			start = s;
			end = e;
			percent = 0;
		}
	}
	
	private Tween currentTween = null;
	private bool isTweening = false;
	
	public bool Revert()
	{
		if (history.Count == 0)
		{
			return false;
		}
		VoxelState st = history.Pop();
		
		currentTween = new Tween(new VoxelState(transform.localScale, transform.position, gameObject.active), st);
		
		isTweening = true;
		gameObject.SetActiveRecursively(true);
		
		return true;
	}
	
	public void Apply(VoxelState st)
	{
		history.Push(new VoxelState(transform.localScale, transform.position, gameObject.active));
		
		currentTween = new Tween(new VoxelState(transform.localScale, transform.position, gameObject.active), st);
		
		
		isTweening = true;
		gameObject.SetActiveRecursively(true);
	}
	
	// This flicker is called prior to level loadings
	// We may call it whenever we want, though...
	public void Flicker(int duration) 
	{
		flickerStartTime = Time.time;
		flickerDuration = duration;
		flicker = true;
	}
	
	
	void Start()
	{
		flickerMaterial = Resources.Load("material/BendZoneMaterial") as Material;
			
		history = new Stack<VoxelState>();
		
		// fix lights
		if (!disableLights)
		{
			Light[] lights = gameObject.GetComponentsInChildren<Light>();
			foreach (Light l in lights)
			{
				Vector3 dir = -transform.position + l.transform.position;
				int layerMask = ~(1<<10 | 1<<2);
				l.enabled = !Physics.Raycast(transform.position, dir, 0.5f, layerMask);
			}
		}
		if (renderer)
			origMaterial = renderer.material;
		else
			origMaterial = null;
	}
	
	void FixedUpdate()
	{
		if (currentTween != null && isTweening && currentTween.percent <= 1.0f)
		{
			Vector3 p0 = currentTween.start.position;
			Vector3 s0 = currentTween.start.scale;
			
			Vector3 p1 = currentTween.end.position;
			Vector3 s1 = currentTween.end.scale;
			
			float t = currentTween.percent;
			t = 3f*t*t + -2f*t*t*t;
			
			transform.position = p0 + t*(p1-p0);
			transform.localScale = s0 + t*(s1-s0);
			
			currentTween.percent += 0.02f;
			
			if(currentTween.percent > 1.0f)
			{
				gameObject.SetActiveRecursively(currentTween.end.active);
				isTweening = false;
				currentTween = null;
				
				// fix lights
				if (!disableLights)
				{
					Light[] lights = gameObject.GetComponentsInChildren<Light>();
			    	foreach (Light l in lights)
					{
						Vector3 dir = -transform.position + l.transform.position;
						int layerMask = ~(1<<10 | 1<<2);
						l.enabled = !Physics.Raycast(transform.position, dir, 2f, layerMask);
					}
				}
				if (origMaterial)
					renderer.material = origMaterial;
			}
		}
		if (isTweening && flicker) // This check is for bending
		{
			Light[] lights = gameObject.GetComponentsInChildren<Light>();
	    	foreach (Light l in lights)
			{
        		l.enabled = false;
			}
			
			Vector3 shake = transform.position + 0.1f*Random.insideUnitSphere;
			transform.position = shake;
			
			if (origMaterial)
			{
				if (Random.value<0.1)
				{
					renderer.material = flickerMaterial;
				} 
				else if(Random.value<0.1)
				{
					renderer.material = origMaterial;
				}
			}
		}
		
		// This flicker has a different check, because bending flicker is based on position(VoxelState)
		// This one based on duration
		if(flicker)
		{
			if(Time.time - flickerStartTime < flickerDuration)
			{
				StartCoroutine(FlickerCoroutine(isFlickerMaterial));
				if(Random.value < 0.1f)
				{
					isFlickerMaterial = !isFlickerMaterial;
				}
			}
			else
			{
				flicker = false;
				renderer.material = origMaterial;
			}
		}
		
	}
	
	IEnumerator FlickerCoroutine(bool isFlickerMaterial)
	{
		if(isFlickerMaterial)
		{
			renderer.material = flickerMaterial;
		}
		else
		{
			int matIndex = Mathf.FloorToInt(Random.value * BlockScript.Materials.Length);
			renderer.material = BlockScript.Materials[matIndex];
			//if(Input.GetKey("t")) print(renderer.material);
			
		}
		yield return null;
	}
}

