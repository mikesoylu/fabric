using UnityEngine;
using System.Collections;

public class TurretLightScript : MonoBehaviour
{
	void Update()
	{
		float s = (Mathf.Sin(Time.time*50.0f)+1)*0.25f;
		light.intensity = s;
	}
}
