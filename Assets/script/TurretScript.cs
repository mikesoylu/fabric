using UnityEngine;
using System.Collections;

public class TurretScript : MonoBehaviour
{
	public Transform Effects;
	private Transform cameraController;
	private Transform player = null;
	
	private float delayCounter = 0;
	private const float delay = 1f;
	
	private bool isHitting = false;
	private float hitCounter = 0;
	private float hitDelay = 0.8f;
	
	
	void Start()
	{
		Effects.gameObject.SetActiveRecursively(false);
		audio.volume = 0f;
	}
	public void SetPlayer(Transform pl)
	{
		player = pl;
	}
	public void SetCamera(Transform cam)
	{
		cameraController = cam;	
	}
	
	void Update()
	{
		isHitting = false;
		hitCounter += Time.deltaTime;
		
		if (player == null)
			return;
		
		Vector3 pp = player.transform.position;
		pp.y += 0.2f;
		Vector3 dd = pp - transform.position;
		Quaternion rot = Quaternion.FromToRotation(Vector3.forward, dd);
		rot.eulerAngles = new Vector3(rot.eulerAngles.x+Random.Range(-0.1f,0.1f),rot.eulerAngles.y+Random.Range(-0.1f,0.1f),0);
		transform.localRotation = rot;
		
		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit))
			if (hit.transform.tag == "player")
			{
				delayCounter += Time.deltaTime;
				if (delayCounter>delay)
				{
					Effects.gameObject.SetActiveRecursively(true);
					isHitting = true;
					audio.volume = 1f;
				}
			} else
			{
				delayCounter = 0;
				Effects.gameObject.SetActiveRecursively(false);
				isHitting = false;
				audio.volume = 0f;
			}
		if (isHitting && hitCounter >= Random.Range(0.5f,1f)*hitDelay)
		{
			hitCounter = 0;
			cameraController.GetComponent<CustomMouseLook>().Hit();
		}
	}
}
