using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;

public class MapScript : MonoBehaviour {
	
	public List<Transform> voxels;
	public Transform playa; 
	
	private LevelLoader_v3 levelLoaderV3;
	private const int initialGlitchingDuration = 3;
	
	void Start()
	{
		string path;
		if(GameObject.Find("IsTestingLevel"))
		{
			path = Application.dataPath + "/Resources/levels/testLevel.txt";
		}
		else
		{
			path = Application.dataPath + "/Resources/levels/flicker_test.txt"; // bu level'dan level'a degiscek.
		}
		
		levelLoaderV3 = gameObject.GetComponent<LevelLoader_v3>();
		
		voxels = levelLoaderV3.LoadFromFile(path, false);
		StartCoroutine(StartGlitchCoroutine());
		
	}
	
	void Update()
	{
		
		/*
		// Debug
		if(Input.GetKeyDown("f"))
		{
			GlitchLevel();
		}
		*/
	}
	
	public bool ShiftX(float lower, float upper, int typeA = 0, int typeB = 0)
	{
		float eps = 0.01f;
		Plane low = new Plane(new Vector3(1,0,0), new Vector3(lower+eps,0,0));
		Plane hi = new Plane(new Vector3(1,0,0), new Vector3(upper-eps,0,0));
		
		if(playa.position.x > lower && playa.position.x < upper)
		{
			GameObject.Find("BloodFlash").GetComponent<BloodScript>().kill();
			return false;
		}
		
		foreach(Transform vx in voxels)
		{
			VoxelScript vs = vx.gameObject.GetComponent<VoxelScript>();
			BlockInfo bi = vx.gameObject.GetComponent<BlockInfo>();
			GameObject go = vx.gameObject;
			
			if(bi.layer == typeA || bi.layer == typeB)
			{
				float sa = 0.5f;
				float sb = 0.5f;
				
				if (typeA < 0 || typeB < 0)
				{
					// Block is nigga.
					sb = 1f;
					sa = 1f;
				}
				if (!low.GetSide(vx.position))
				{
					vs.Apply(new VoxelState(vx.localScale,
									new Vector3(vx.position.x-(lower - upper + 1)*sa, vx.position.y, vx.position.z),
									go.active));
					vs.flicker = false;
					
				} else if (hi.GetSide(vx.position))
				{
					
					 vs.Apply(new VoxelState(vx.localScale,
									new Vector3(vx.position.x+(lower - upper + 1)*sb, vx.position.y, vx.position.z),
									go.active));
					vs.flicker = false;
				} else
				{
					vs.Apply(new VoxelState(new Vector3(0f, vx.localScale.y, vx.localScale.z),
									new Vector3((lower + upper)*0.5f, vx.position.y, vx.position.z),
									false));
					vs.flicker = true;
				}
			}
			
		}
		return true;
	}
	public bool ShiftY(float lower, float upper, int typeA = 0, int typeB = 0)
	{
		float eps = 0.01f;
		Plane low = new Plane(new Vector3(0,1,0), new Vector3(0,lower+eps,0));
		Plane hi = new Plane(new Vector3(0,1,0), new Vector3(0,upper-eps,0));
		
		if(playa.position.y > lower && playa.position.y < upper)
		{
			GameObject.Find("BloodFlash").GetComponent<BloodScript>().kill();
			return false;
		}
		
		
		foreach(Transform vx in voxels)
		{
			VoxelScript vs = vx.gameObject.GetComponent<VoxelScript>();
			BlockInfo bi = vx.gameObject.GetComponent<BlockInfo>();
			GameObject go = vx.gameObject;
			
			if(bi.layer == typeA || bi.layer == typeB)
			{
				float sa = 0.5f;
				float sb = 0.5f;
				
				if (typeA < 0 || typeB < 0)
				{
					// Block is black.
					sb = 1f;
					sa = 1f;
				}
				if (!low.GetSide(vx.position))
				{
					vs.Apply(new VoxelState(vx.localScale,
									new Vector3(vx.position.x, vx.position.y-(lower - upper + 1)*sa, vx.position.z),
									go.active));
					vs.flicker = false;
					
				} else if (hi.GetSide(vx.position))
				{
					vs.Apply(new VoxelState(vx.localScale,
									new Vector3(vx.position.x, vx.position.y+(lower - upper + 1)*sb, vx.position.z),
									go.active));
					vs.flicker = false;
				} else
				{
					vs.Apply(new VoxelState(new Vector3(vx.localScale.x, 0f, vx.localScale.z),
									new Vector3(vx.position.x, (lower + upper)*0.5f, vx.position.z),
									false));
					vs.flicker = true;
				}
			}
		}
		return true;
	}
	public bool ShiftZ(float lower, float upper, int typeA = 0, int typeB = 0)
	{
		float eps = 0.01f;
		Plane low = new Plane(new Vector3(0,0,1), new Vector3(0,0,lower+eps));
		Plane hi = new Plane(new Vector3(0,0,1), new Vector3(0,0,upper-eps));
		
		if(playa.position.z > lower && playa.position.z < upper)
		{
			GameObject.Find("BloodFlash").GetComponent<BloodScript>().kill();
			return false;
		}
		
		foreach(Transform vx in voxels)
		{
			VoxelScript vs = vx.gameObject.GetComponent<VoxelScript>();
			BlockInfo bi = vx.gameObject.GetComponent<BlockInfo>();
			GameObject go = vx.gameObject;
			
			if(bi.layer == typeA || bi.layer == typeB)
			{
				float sa = 0.5f;
				float sb = 0.5f;
				
				if (typeA < 0 || typeB < 0)
				{
					// Block is black.
					sb = 1f;
					sa = 1f;
				}
				if (!low.GetSide(vx.position))
				{
					vs.Apply(new VoxelState(vx.localScale,
									new Vector3(vx.position.x, vx.position.y, vx.position.z-(lower - upper + 1)*sa),
									go.active));
					vs.flicker = false;
					
				} else if (hi.GetSide(vx.position))
				{
					vs.Apply(new VoxelState(vx.localScale,
									new Vector3(vx.position.x, vx.position.y, vx.position.z+(lower - upper + 1)*sb),
									go.active));
					vs.flicker = false;
				} else
				{
					vs.Apply(new VoxelState(new Vector3(vx.localScale.x, vx.localScale.y, 0f),
									new Vector3(vx.position.x, vx.position.y, (lower + upper)*0.5f),
									false));
					vs.flicker = true;
				}
			}
		}
		return true;
	}
	public bool RevertMap()
	{
		bool ret = false;
		int i = 0;
		foreach(Transform vx in voxels)
		{
			i++;
			if(vx == null) print (i+"!!!");
			ret = vx.gameObject.GetComponent<VoxelScript>().Revert();
		}
		return ret;
	}
	
	public bool PressButton()
	{
		if (levelLoaderV3.BUTTON_PRESSED)
		{
			print("already pressed");
			return false;
		}
		
		levelLoaderV3.BUTTON_PRESSED = true;
		
		Transform endTile = levelLoaderV3.endTile;
		Vector3 endTilePos = endTile.transform.position;
		
		for (int i = 1; i<5; i++)
		{
			Object nv = Instantiate(BlockScript.EndTilePrefab,
						 new Vector3(endTilePos.x,endTilePos.y+i,endTilePos.z),
						 Quaternion.identity);
			((Transform)nv).transform.localEulerAngles = new Vector3(-90,0,0);
			((Transform)nv).transform.parent = endTile;
			// only do it for the first
			if (i == 1)
			{
				((Transform)nv).transform.tag = "ender";
			}
			((Transform)nv).collider.isTrigger = true;
			
		}
		return true;
	}
	
	public void AcquireGun()
	{
		LevelLoader_v3.GUN_ACQUIRED = true;
		voxels.Remove(levelLoaderV3.gunTile);
		Destroy(levelLoaderV3.gunTile.gameObject);
	}
	
	void GlitchLevel(int flickerCount, int flickerDuration)
	{
		
		for(int i = 0; i < flickerCount; i++)
		{
			int flickerIndex = Mathf.FloorToInt(Random.value * voxels.Count);
			voxels[flickerIndex].GetComponent<VoxelScript>().Flicker(flickerDuration);
		}
	}
	
	IEnumerator StartGlitchCoroutine()
	{
		GlitchLevel(voxels.Count / 5, 1);
		yield return new WaitForSeconds(1);
		GlitchLevel(voxels.Count / 7, 2);
		yield return new WaitForSeconds(1);
		GlitchLevel(voxels.Count / 10, 1);
	}
	
}
